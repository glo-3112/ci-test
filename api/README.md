# UGRAM API

## Installation 
#### Notes
1. Assurez-vous d'avoir le dossier `ugram` (contenant `backend` et `frontend`) dans le dossier `$GOPATH/src` (probablement ~/go/src/ugram)

Lancez les commandes suivantes : 
1. `cd backend`
2. `go mod download`
3. `go run ./src`

## Workflow
### Routing 
Les routes sont gérées via un tissu de middleware. Tous les controlleurs sont groupés dans des fichiers dédiés. 
L'arboressence de ces fichiers suis la route. 
Exemple :
```http request
/api/v1/users
```
Admettons que cette route gère la liste des utilisateurs. On aurait donc cette architecture :
```
api/
  | - init.go
  \ - v1/
       | - init.go
       \ - users/
            | - init.go
            \ - users.go
```
* Le fichier `api/init.go` contient un routeur dédié aux routes `/api`
* Le fichier `api/v1/init.go` contient un routeur dédié aux routes `/v1`
* Le fichier `api/v1/users/init.go` contient un routeur dédié aux routes de `/users`
* Le fichier `api/v1/users/user.go` contient les controlleurs dédiés aux routes s`/users/*`
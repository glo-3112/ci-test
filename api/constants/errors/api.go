package errors

const (
	ErrNoBodyProvided = "ERROR_NO_BODY_PROVIDED"
	ErrBadBodyFormat  = "ERROR_WRONG_BODY_FORMAT"
)

package constants

import (
	"os"
)

type Size struct {
	Width  uint64
	Height uint64
}

var staticDir = ""

const (
	ImageSmallWidth   = 400
	ImageSmallHeight  = 400
	ImageMediumWidth  = 800
	ImageMediumHeight = 800
	ImageLargeWidth   = 1500
	ImageLargeHeight  = 1500
)

func GetAllowedImageMimeTypes() []string {
	return []string{
		"image/jpeg",
		"image/png",
		"image/tiff",
		"image/gif",
		"image/bmp",
		"image/webp",
	}
}

func GetStaticDir( ) string {
	return staticDir
}

func init() {
	staticDir = os.Getenv(StaticDirectory)
}

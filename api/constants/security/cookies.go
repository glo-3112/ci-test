package constants

import (
	"time"
)

const (
	// Expiration in days
	TokenExpiration     = 1
	TokenCookieName     = "token"
	TokenCookieSecure   = false
	TokenCookieHttpOnly = false
	TokenCookiePath     = "/"
	TokenCookieExpiry   = time.Hour * 24

	// Expiration in days
	AuthenticationExpiration     = TokenExpiration
	AuthenticationCookieName     = "authenticated"
	AuthenticationCookieSecure   = false
	AuthenticationCookieHttpOnly = false
	AuthenticationCookiePath     = TokenCookiePath
	AuthenticationCookieExpiry   = TokenCookieExpiry
)

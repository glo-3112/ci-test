package api

import (
	"api/controllers/api/v1/auth"
	"api/controllers/api/v1/publications"
	"api/controllers/api/v1/users"
	"api/router"
)

var ApiRouter *router.Router

func init() {
	ApiRouter = router.NewRouter("/api/v1")
	ApiRouter.Use(auth.Router)
	ApiRouter.Use(publications.Router)
	ApiRouter.Use(users.Router)
}

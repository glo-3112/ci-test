package auth

import (
	"net/http"

	. "api/middlewares"
	"api/router"
)

var Router *router.Router

func init() {
	Router = router.NewRouter("/auth")

	Router.Register("/sign-in", CheckMethod(http.MethodPost), PostSignInController)
	Router.Register("/sign-up", CheckMethod(http.MethodPost), PostSignUpController)
	Router.Register("/sign-out", CheckMethod(http.MethodPost, http.MethodGet, http.MethodDelete), ShouldBeAuthenticated, SignOutController)
}

package auth

import (
	"net/http"
	"time"

	constants "api/constants/security"
	form "api/form/api/v1"
	"api/generic/api"
	"api/repository"
	"api/req"
	. "api/tools"
	"api/tools/console"

	security "api/security/jwt"
)

// Description :
//		Method POST
//		This controller register a user using the given body
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func PostSignInController(s *req.Session, w req.Writer, r *req.Reader, _ CtrlFunc) {
	// Parsing json body
	formData := form.SignIn{}
	if err := r.UnmarshalJSON(&formData); err != nil {
		console.Log(err)
		api.SendBadRequest(w, "Can't read JSON body")
		return
	}

	// Looking if the user is existing or not
	user, err := repository.UserRepository.FindByLoginAndPassword(formData.Login, formData.Password)
	if err != nil {
		console.Log(err)
		api.SendForbidden(w, "No combination login password found")
		return
	}

	err = repository.UserRepository.UpdateLastSignIn(user)
	if err != nil {
		console.Log(err)
		api.SendForbidden(w, "No combination login password found 2")
		return
	}

	// Making the token and creating associated cookies
	jwt := security.CreateJwt(*user)

	token := &http.Cookie{
		Name:     constants.TokenCookieName,
		Value:    jwt,
		HttpOnly: constants.TokenCookieHttpOnly,
		Secure:   constants.TokenCookieSecure,
		// MaxAge:   constants.TokenCookieMaxAge,
		Expires: time.Now().Add(constants.TokenCookieExpiry),
		Path:    constants.TokenCookiePath,
	}
	auth := &http.Cookie{
		Name:     constants.AuthenticationCookieName,
		Value:    "true",
		HttpOnly: constants.AuthenticationCookieHttpOnly,
		Secure:   constants.AuthenticationCookieSecure,
		// MaxAge:   constants.AuthenticationMaxAge,
		Expires: time.Now().Add(constants.AuthenticationCookieExpiry),
		Path:    constants.AuthenticationCookiePath,
	}

	// Setting cookies
	http.SetCookie(w, token)
	http.SetCookie(w, auth)

	// Respond to client
	api.Send(w, user)
}

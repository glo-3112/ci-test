package auth

import (
	"net/http"

	constants "api/constants/security"
	"api/generic/api"
	"api/repository"
	"api/req"
	"api/tools"
	"api/tools/console"
)

// Description :
//		Method POST
//		This controller sign out a user
// Parameters :
//      s *req.Session :
//          The current user session (Can't be equal to nil)
//		w req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func SignOutController(s *req.Session, w req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	// Getting the token cookie
	token, err := r.Cookie(constants.TokenCookieName)
	if err != nil {
		console.Log("Can't read token cookie", err)
		api.SendUnauthorized(w, "Bad token")
		return
	}
	// Getting the authentication cookie marker
	auth, err := r.Cookie(constants.AuthenticationCookieName)
	if err != nil {
		console.Log("Can't read authentication cookie", err)
		api.SendUnauthorized(w, "Bad token 2")
	}

	err = repository.TokenRepository.DeleteById(s.Token.Id)
	if err != nil {
		console.Log(err)
		api.SendInternalError(w, http.StatusText(http.StatusInternalServerError))
	}
	// Setting age to 1 (has the effect of deleting the cookie)
	token = &http.Cookie{
		Name:     constants.TokenCookieName,
		Value:    "",
		HttpOnly: constants.TokenCookieHttpOnly,
		MaxAge:   1,
		Path:     constants.TokenCookiePath,
	}
	auth = &http.Cookie{
		Name:     constants.AuthenticationCookieName,
		Value:    "true",
		HttpOnly: constants.AuthenticationCookieHttpOnly,
		MaxAge:   1,
		Path:     constants.AuthenticationCookiePath,
	}

	// Setting cookies update
	http.SetCookie(w, token)
	http.SetCookie(w, auth)

	// Respond to client
	api.Send(w, nil, "Successfully disconnected")
}

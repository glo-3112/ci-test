package auth

import (
	"net/http"

	form "api/form/api/v1"
	"api/generic/api"
	"api/repository"
	"api/req"
	"api/security"
	"api/tools"
	"api/tools/console"
	"api/tools/validators"
)

// Description :
//		Method POST
//		This controller sign in a user using the given body
// Parameters :
//      s *req.Session :
//          The current user session (Can't be equal to nil)
//		w req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func PostSignUpController(s *req.Session, w req.Writer, r *req.Reader, _ tools.CtrlFunc) {

	// Getting request body

	// Parsing json body
	form := form.SignUp{}
	if err := r.UnmarshalJSON(&form); err != nil {
		console.Log(err)
		api.SendBadRequest(w, "Can't parse JSON body")
		return
	}

	if err := security.PasswordStrength(form.Password); err != nil {
		api.SendBadRequest(w, err.Error())
		return
	}

	if err := validators.IsMailValid(form.Mail); err != nil {
		api.SendBadRequest(w, err.Error())
		return
	}

	// Verifying the phone number
	if err := validators.IsPhoneValid(&form.PhoneNumber); err != nil {
		api.SendBadRequest(w, err.Error())
		return
	}

	// Looking if the user is existing or not
	status, err := repository.UserRepository.IsUsernameOrMailAvailable(form.Username, form.Mail)
	if err != nil {
		console.Log(err)
		api.SendInternalError(w, "An unknown error occured")
		return
	}

	if status == false {
		api.SendResponse(w, http.StatusConflict, "Username or mail address already taken", nil)
		return
	}

	_, err = repository.UserRepository.CreateUserNoHashPassword(form)
	if err != nil {
		console.Log(err)
		api.SendInternalError(w, "An unknown error occurred")
		return
	}

	// Respond to client
	api.Send(w, nil)
	return
}

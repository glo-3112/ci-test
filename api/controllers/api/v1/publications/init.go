package publications

import (
	. "api/middlewares"
	"api/router"
)

var Router *router.Router

func init() {
	Router = router.NewRouter("/publications")

	Router.Register("/", MultiMethod(Methods{
		"POST": {ShouldBeAuthenticated, PostPublicationController},
	}))

	Router.Register("/{publicationId}", MultiMethod(Methods{
		"GET":   {ResolveAuthentication, ResolvePublication, GetSinglePublicationController},
		"PATCH": {ShouldBeAuthenticated, ShouldOwnPublication, PatchSinglePublicationController},
	}))

	Router.Register("/{publicationId}/images", MultiMethod(Methods{
		"GET":    {ResolveAuthentication, ResolvePublication, GetPublicationImagesController},
		"POST":   {ShouldBeAuthenticated, ShouldOwnPublication, PostPublicationImagesController},
		"DELETE": {ShouldBeAuthenticated, ShouldOwnPublication, DeletePublicationImagesController},
	}))

	Router.Register("/{publicationId}/images/{imageFormat:(?:original|medium|small)}/{imageId:.*}", MultiMethod(Methods{
		"GET":    {ResolveAuthentication, ResolvePublication, ServePublicationPictureController},
	}))

	// Router.Register("/{publicationId}/images")
	// Router.Register("/publications/{postId}/images/{imageSize}/{imageId:.*}", CheckMethod("GET"), static.GetPublicationPictureController)
}

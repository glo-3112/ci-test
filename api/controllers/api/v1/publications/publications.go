package publications

import (
	"net/http"

	"api/generic/api"
	"api/models"
	"api/repository"
	"api/req"
	"api/tools"
	"api/tools/console"
)

func PostPublicationController(s *req.Session, w req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	pub := models.PublicationCreate{}

	err := r.UnmarshalJSON(&pub)
	if err != nil {
		console.Log(err)
		api.SendBadRequest(w, "Can't parse JSON body")
		return
	}

	publication, err  := repository.PublicationsRepository.CreatePublication(s.User, pub)
	if err != nil {
		console.Log(err)
		api.SendInternalError(w, "An unknown error occurred")
		return
	}

	api.SendResponse(w, http.StatusCreated, http.StatusText(http.StatusCreated), publication)
}

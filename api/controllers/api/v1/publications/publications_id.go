package publications

import (
	"net/http"
	"time"

	"gopkg.in/mgo.v2"

	"api/generic/api"
	"api/models"
	"api/repository"
	"api/req"
	"api/tools"
	"api/tools/console"
)

func GetSinglePublicationController(_ *req.Session, w req.Writer, r *req.Reader, _ tools.CtrlFunc) {

	publication := r.SolvedParams["publication"]

	api.SendResponse(w, http.StatusOK, http.StatusText(http.StatusOK), publication)
}

func PatchSinglePublicationController(s *req.Session, w req.Writer, r *req.Reader, _ tools.CtrlFunc) {

	author := s.User

	publicationId := r.Params["publicationId"]
	if publicationId == "" {
		api.SendNotFound(w, "Publication", publicationId, "not found")
		return
	}

	publication, err := repository.PublicationsRepository.FindByAuthorAndId(author.Id.Hex(), publicationId)
	if err != nil {
		console.Log(err)
		api.SendInternalError(w, "Unknown error occurred")
		return
	} else if publication == nil {
		api.SendNotFound(w, "Publication", publicationId, "not found")
		return
	}

	patch := models.Publication{}
	err = r.UnmarshalJSON(&patch)
	if err != nil {
		console.Log(err)
		api.SendBadRequest(w, "Can't parse body")
		return
	}

	if patch.MentionsId != nil {
		count, err := repository.UserRepository.CountUserIds(patch.MentionsId)
		if err != nil && err != mgo.ErrNotFound {
			console.Log(err)
			api.SendInternalError(w, "An unknown error occurred")
			return
		}
		if count != len(patch.MentionsId) {
			api.SendNotFound(w, "Not all users found")
			return
		}
		publication.MentionsId = patch.MentionsId
	}
	if patch.Description != nil {
		publication.Description = patch.Description
	}
	if patch.Tags != nil {
		publication.Tags = patch.Tags
	}
	publication.UpdatedAt = time.Now()
	publication, err = repository.PublicationsRepository.Persist(publication)
	if err != nil {
		console.Log(err)
		api.SendInternalError(w)
		return
	}

	api.SendResponse(w, http.StatusOK, http.StatusText(http.StatusOK), publication)
}

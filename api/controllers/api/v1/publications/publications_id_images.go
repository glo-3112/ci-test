package publications

import (
	"fmt"
	"net/http"
	"path"

	"gopkg.in/mgo.v2/bson"

	"api/constants"
	"api/generic/api"
	"api/generic/static"
	"api/models"
	"api/repository"
	"api/req"
	"api/tools"
	"api/tools/console"
	"api/tools/filemanager"
)

func GetPublicationImagesController(_ *req.Session, w req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	publication := r.SolvedParams["publication"].(*models.Publication)

	api.SendResponse(w, http.StatusOK, http.StatusText(http.StatusOK), publication.Images)
}

func PostPublicationImagesController(s *req.Session, w req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	publication := r.SolvedParams["publication"].(*models.Publication)

	fmt.Println()
	// Max upload size -> 20 MB
	err := r.Req.ParseMultipartForm(20 << 20)

	if err != nil {
		api.SendResponse(w, http.StatusRequestEntityTooLarge, "Max payload -> 20 Mb", nil)
		return
	}

	formData := r.Req.MultipartForm
	files := formData.File["upload"]

	loadedImages := []bson.ObjectId{}

	for _, file := range files {
		fileid := bson.NewObjectId()
		err := filemanager.SavePicture(file, fileid.Hex())
		if err != nil {
			break
		}
		loadedImages = append(loadedImages, fileid)
	}

	publication.Images = append(publication.Images, loadedImages...)
	publication, err = repository.PublicationsRepository.Persist(publication)
	if err != nil {
		api.SendResponse(w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), nil)
		return
	}

	api.SendResponse(w, http.StatusOK, http.StatusText(http.StatusOK), publication)
}

func DeletePublicationImagesController(s *req.Session, w req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	imagesToDelete := []bson.ObjectId{}

	err := r.UnmarshalJSON(&imagesToDelete)
	if err != nil {
		api.SendBadRequest(w, "Bad body format")
		return
	}

	publication := r.SolvedParams["publication"].(*models.Publication)
	files := []string{}

removing:
	for i, v := range imagesToDelete {
		for index, image := range publication.Images {
			if image == v {
				files = append(files, image.Hex())

				// Removing id found
				publication.Images = append(publication.Images[0:index], publication.Images[index+1:]...)
				imagesToDelete = append(imagesToDelete[0:i], imagesToDelete[i+1:]...)
				goto removing
			}
		}
	}

	publication, err = repository.PublicationsRepository.Persist(publication)
	if err != nil {
		api.SendInternalError(w, "An unknown error occurred")
		return
	}

	_ = filemanager.RemovePicturesByFolderAndName("original", files...)
	_ = filemanager.RemovePicturesByFolderAndName("small", files...)
	_ = filemanager.RemovePicturesByFolderAndName("medium", files...)

	api.SendResponse(w, http.StatusOK, http.StatusText(http.StatusOK), publication)
}

func ServePublicationPictureController(s *req.Session, w req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	publication := r.SolvedParams["publication"].(*models.Publication)

	if r.Params["imageId"] == "" {
		console.Log("Param imageId is empty")
		static.RespondError(w, http.StatusNotFound)
		return
	}

	imageId := bson.ObjectIdHex(r.Params["imageId"])
	index := -1
	for i, v := range publication.Images {
		if v == imageId {
			index = i
			break
		}
	}

	if index < 0 {
		console.Log("Image is not found in the publication")
		static.RespondError(w, http.StatusNotFound)
		return
	}

	imageFormat := r.Params["imageFormat"]

	dir := http.Dir(constants.GetStaticDir())

	file, err := dir.Open(path.Join(imageFormat, imageId.Hex() + ".png"))
	if err != nil {
		console.Log("Can't open the image :", path.Join(imageFormat, imageId.Hex() + ".png"))
		static.RespondError(w, http.StatusNotFound)
		return
	}

	infos, err := file.Stat()
	if err != nil {
		console.Log("Can't stat the image :", path.Join(imageFormat, imageId.Hex() + ".png"))
		static.RespondError(w, http.StatusNotFound)
		return
	}

	if !infos.Mode().IsRegular() {
		console.Log("The file", path.Join(imageFormat, imageId.Hex() + ".png"), "is not a regular file")
		static.RespondError(w, http.StatusNotFound)
		return
	}

	static.Respond(w, file, http.StatusOK)
}

package users

import (
	"net/http"

	. "api/middlewares"
	"api/router"
)

var Router *router.Router

func init() {
	Router = router.NewRouter("/users")

	Router.Register("/", CheckMethod(http.MethodGet), GetAllUsersController)
	Router.Register("/search", CheckMethod(http.MethodGet), SearchUserController)

	Router.Register("/{userId}", MultiMethod(Methods{
		http.MethodGet:   {ResolveAuthentication, ResolveMe, ResolveUserParam, GetUserController},
		http.MethodPatch: {ShouldBeAuthenticated, ShouldBeMe, PatchMe},
	}))

	Router.Register("/{userId}/publications", MultiMethod(Methods{
		http.MethodGet: {ResolveAuthentication, ResolveMe, ResolveUserParam, GetUserPublicationsController},
	}))

	Router.Register("/{userId}/firstname", ShouldBeAuthenticated, ShouldBeMe, CheckMethod(http.MethodPatch), PatchMeFirstname)

	Router.Register("/{userId}/lastname", ShouldBeAuthenticated, ShouldBeMe, CheckMethod(http.MethodPatch), PatchMeLastname)

	Router.Register("/{userId}/mail", ShouldBeAuthenticated, ShouldBeMe, CheckMethod(http.MethodPatch), PatchMeMail)

	Router.Register("/{userId}/phone", ShouldBeAuthenticated, ShouldBeMe, CheckMethod(http.MethodPatch), PatchMePhone)
}

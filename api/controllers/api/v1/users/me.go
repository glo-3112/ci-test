package users

import (
	"fmt"
	"net/http"

	"api/constants/errors"
	"api/generic/api"
	"api/models"
	"api/repository"
	"api/req"
	. "api/tools"
	"api/tools/console"
	"api/tools/validators"
)

func PatchMe(s *req.Session, w req.Writer, r *req.Reader, _ CtrlFunc) {
	form := &models.PartialUser{}

	if err := r.UnmarshalJSON(form); err != nil {
		api.SendBadRequest(w, err.Error())
		return
	}

	me := s.User
	if form.Name != nil && *form.Name != "" {
		me.Name = *form.Name
	}
	if form.Firstname != nil && *form.Firstname != "" {
		me.Firstname = *form.Firstname
	}
	if form.Lastname != nil && *form.Lastname != "" {
		me.Lastname = *form.Lastname
	}
	if form.Mail != nil && *form.Mail != "" {
		me.Mail = *form.Mail
	}
	if form.PhoneNumber != nil {
		err := validators.IsPhoneValid(form.PhoneNumber)
		if err != nil {
			api.SendBadRequest(w, err.Error())
			return
		}
		me.PhoneNumber = *form.PhoneNumber
	}

	err := repository.UserRepository.Persist(me)
	if err != nil {
		api.SendInternalError(w, "An unknown error occurred")
		return
	}

	api.Send(w, me)
}

// Description :
//		Method Patch
//		Update the firstname of the authenticated user
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func PatchMeFirstname(s *req.Session, w req.Writer, r *req.Reader, _ CtrlFunc) {

	form := &models.UserFirstname{}

	if err := r.UnmarshalJSON(form); err != nil {
		api.SendBadRequest(w, err.Error())
		return
	}

	if form.Firstname == "" {
		api.SendResponse(w, http.StatusBadRequest, errors.ErrFirstnameEmpty, nil)
		return
	}

	user := s.Token.User
	user.Firstname = form.Firstname

	if err := repository.UserRepository.Persist(user); err != nil {
		fmt.Println(err)
		api.SendResponse(w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), nil)
		return
	}

	api.Send(w, user)
}

// Description :
//		Method Patch
//		Update the lastname of the authenticated user
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func PatchMeLastname(s *req.Session, w req.Writer, r *req.Reader, _ CtrlFunc) {

	form := models.UserLastname{}

	if err := r.UnmarshalJSON(&form); err != nil {
		api.SendBadRequest(w, err.Error())
		return
	}

	if form.Lastname == "" {
		api.SendBadRequest(w, errors.ErrLastnameEmpty)
		return
	}

	user := s.Token.User
	user.Lastname = form.Lastname

	if err := repository.UserRepository.Persist(user); err != nil {
		fmt.Println(err)
		api.SendInternalError(w, "An unknown error occurred")
		return
	}

	api.Send(w, user)
}

//		Method Patch
//		Update the email of the authenticated user
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func PatchMeMail(s *req.Session, w req.Writer, r *req.Reader, _ CtrlFunc) {

	form := models.UserMail{}

	if err := r.UnmarshalJSON(&form); err != nil {
		api.SendBadRequest(w, err.Error())
		return
	}

	if form.Mail != s.Token.User.Mail {
		if form.Mail == "" {
			api.SendBadRequest(w, errors.ErrMailEmpty)
			return
		}

		if err := validators.IsMailValid(form.Mail); err != nil {
			api.SendBadRequest(w, err.Error())
			return
		}

		if status, err := repository.UserRepository.IsMailAvailable(form.Mail); err != nil {
			api.SendInternalError(w)
			return
		} else if !status {
			api.SendResponse(w, http.StatusConflict, errors.ErrMailAlreadyUsed, nil)
			return
		}

		user := s.Token.User
		user.Mail = form.Mail

		if err := repository.UserRepository.Persist(user); err != nil {
			fmt.Println(err)
			api.SendInternalError(w)
			return
		}
	}

	api.Send(w, s.User)
}

// Description :
//		Method Patch
//		Update the user phone number of the authenticated user
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func PatchMePhone(s *req.Session, w req.Writer, r *req.Reader, _ CtrlFunc) {

	form := models.PhoneNumber{}
	if err := r.UnmarshalJSON(&form); err != nil {
		console.Log(err)
		api.SendBadRequest(w, "Can't parse JSON body")
		return
	}

	user := s.User

	if form.Number != user.PhoneNumber.Number || form.ISOCode != user.PhoneNumber.ISOCode {
		if err := validators.IsPhoneValid(&form); err != nil {
			api.SendBadRequest(w, err.Error())
			return
		}

		if status, err := repository.UserRepository.IsPhoneAvailable(form); err != nil {
			console.Log(err)
			api.SendInternalError(w)
			return
		} else if status == false {
			api.SendResponse(w, http.StatusConflict, "This phone number is already taken", nil)
			return
		}

		user.PhoneNumber = form

		if err := repository.UserRepository.Persist(user); err != nil {
			console.Log(err)
			api.SendInternalError(w)
			return
		}
	}
	api.Send(w, s.User)
}

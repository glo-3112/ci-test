package users

import (
	"api/generic/api"
	"api/models"
	"api/repository"
	"api/req"
	"api/tools"
	"api/tools/console"
)

func GetUserPublicationsController(s *req.Session, w req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	page, size := r.Paged(1, 20)

	author := r.SolvedParams["user"].(*models.User)

	publications, err := repository.PublicationsRepository.FindUserPublicationsPaged(author.Id, page, size)
	if err != nil {
		console.Log(err)
		api.SendInternalError(w, "Unknown error occured")
		return
	}

	api.Send(w, r.MakePagedResponse(page, size, publications))
}


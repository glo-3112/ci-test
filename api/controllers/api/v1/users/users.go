package users

import (
	"api/generic/api"
	"api/models"
	"api/repository"
	"api/req"
	"api/tools"
	"api/tools/console"
)

// Description :
//		Method GET
//		This controller return all the registered users
// Parameters :
//      _ *req.Session :
//          The current user session (Can't be equal to nil)
//		w req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func GetAllUsersController(_ *req.Session, w req.Writer, r *req.Reader, _ tools.CtrlFunc) {

	page, size := r.Paged(1, 20)

	users, _ := repository.UserRepository.PublicFindPaged(page, size)

	api.Send(w, r.MakePagedResponse(page, size, users))
}

func SearchUserController(_ *req.Session, w req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	q := r.Req.URL.Query().Get("q")

	page, size := r.Paged(1, 10)

	users, err := repository.UserRepository.PublicUserSearchPaged(q, page, size)
	if err != nil {
		console.Log(err)
		api.SendInternalError(w)
		return
	}
	api.Send(w, r.MakePagedResponse(page, size, users))
}

func GetUserController(s *req.Session, w req.Writer, r *req.Reader, _ tools.CtrlFunc) {

	if r.SolvedParams["target_user"] == nil {
		api.SendNotFound(w, "User", r.Params["userId"], "can't be found")
		return
	}
	targetUser := r.SolvedParams["target_user"].(*models.User)

	if s.User != nil {
		if s.User.Id == targetUser.Id {
			api.Send(w, targetUser)
			return
		}
	}
	if targetUser.Public {
		public := repository.UserRepository.GetPublicData(targetUser)
		api.Send(w, public)
		return
	}
	public := repository.UserRepository.GetPrivacyPublicData(targetUser)
	api.Send(w, public)
}

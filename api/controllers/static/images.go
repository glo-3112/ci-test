package static

import (
	"net/http"
	// "path"

	"api/constants"
	"api/generic/static"
	"api/req"
	"api/tools"
)

func GetStaticImages(s *req.Session, w req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	staticPath := r.Params["path"]

	dir := http.Dir(constants.GetStaticDir())

	file, err := dir.Open(staticPath)
	if err != nil {
		static.RespondError(w, http.StatusNotFound)
		return
	}

	infos, err := file.Stat()
	if err != nil {
		static.RespondError(w, http.StatusNotFound)
		return
	}

	if !infos.Mode().IsRegular() {
		static.RespondError(w, http.StatusNotFound)
		return
	}

	static.Respond(w, file, http.StatusOK)
}

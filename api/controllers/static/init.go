package static

import (
	. "api/router"
)

var StaticRouter *Router

func init() {
	StaticRouter = NewRouter("/static")
	// StaticRouter.Register("/post", CheckMethod("POST"), SmallThumbnailController)
	// StaticRouter.Register("/images/{path:(?:small|original|medium)/.*}", MultiMethod(Methods{
	// 	http.MethodGet: {GetStaticImages},
	// }))
}

package static

import (
	"api/generic/api"
	"api/req"
	. "api/tools"
)

func NotImplementedController(s *req.Session, w req.Writer, r *req.Reader, next CtrlFunc) {
	api.SendResponse(w, 501, "Not implmented yet", nil)
}
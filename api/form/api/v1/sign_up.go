package form

import (
	"api/models"
)

type SignUp struct {
	Username    string             `json:"username"`
	Password    string             `json:"password"`
	Mail        string             `json:"mail,omitempty"`
	Firstname   string             `json:"firstname"`
	Lastname    string             `json:"lastname"`
	PhoneNumber models.PhoneNumber `json:"phoneNumber"`
}

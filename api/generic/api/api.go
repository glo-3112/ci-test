package api

import (
	"net/http"
	"strings"

	"api/req"
)

type ApiError struct {
	Code    int
	Message string
}

func SendError(w req.Writer, err ApiError) {
	SendResponse(w, err.Code, err.Message, nil)
}

func SendNotImplemented(w req.Writer, message ...string) {
	if len(message) > 0 {
		SendResponse(w, http.StatusNotImplemented, strings.Join(message, " "), nil)
	} else {
		SendResponse(w, http.StatusNotImplemented, http.StatusText(http.StatusNotImplemented), nil)
	}
}

func NoBodyProvided(w req.Writer) {
	SendResponse(w, http.StatusBadRequest, "You must provide a body", nil)
}

func SendForbidden(w req.Writer, message ...string) {
	if len(message) > 0 {
		SendResponse(w, http.StatusForbidden, strings.Join(message, " "), nil)
	} else {
		SendResponse(w, http.StatusForbidden, http.StatusText(http.StatusForbidden), nil)
	}
}

func SendNotFound(w req.Writer, message ...string) {
	if len(message) > 0 {
		SendResponse(w, http.StatusNotFound, strings.Join(message, " "), nil)
	} else {
		SendResponse(w, http.StatusNotFound, http.StatusText(http.StatusNotFound), nil)
	}
}

func SendBadRequest(w req.Writer, message ...string) {
	if len(message) > 0 {
		SendResponse(w, http.StatusBadRequest, strings.Join(message, " "), nil)
	} else {
		SendResponse(w, http.StatusBadRequest, http.StatusText(http.StatusBadRequest), nil)
	}
}

func SendInternalError(w req.Writer, message ...string) {
	if len(message) > 0 {
		SendResponse(w, http.StatusInternalServerError, strings.Join(message, " "), nil)
	} else {
		SendResponse(w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), nil)
	}
}

func SendUnauthorized(w req.Writer, message ...string) {
	if len(message) > 0 {
		SendResponse(w, http.StatusUnauthorized, strings.Join(message, " "), nil)
	} else {
		SendResponse(w, http.StatusUnauthorized, http.StatusText(http.StatusUnauthorized), nil)
	}
}

func Send(w req.Writer, data interface{}, message ...string) {
	if len(message) > 0 {
		SendResponse(w, http.StatusUnauthorized, strings.Join(message, " "), data)
	} else {
		SendResponse(w, http.StatusOK, http.StatusText(http.StatusOK), data)
	}
}
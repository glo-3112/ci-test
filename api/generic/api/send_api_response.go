package api

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"api/req"
)

func SendResponse(w req.Writer, status int, message string, payload interface{}) {
	w.Header().Set("Content-Type", "application/json")

	w.WriteHeader(status)
	resp := &req.ApiResponse{Code: status, Message: message, Data: payload, ExecutionTime: fmt.Sprint(time.Since(w.RequestTime)) }

	r, err := json.Marshal(resp)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
	}
	_, _ = w.Write(r)
}

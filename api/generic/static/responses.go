package static

import (
	"io"
	"net/http"
	"strconv"

	"api/req"
)

func RespondError(w req.Writer, status int) {
	w.WriteHeader(status)
	_, _ = w.Write([]byte{})
}

func Respond(w req.Writer, file http.File, status int) {

	fileHeader := make([]byte, 512)
	_, err := file.Read(fileHeader)
	if err != nil {
		RespondError(w, http.StatusInternalServerError)
	}

	fileContentType := http.DetectContentType(fileHeader)

	stats, _ := file.Stat()
	size := strconv.FormatInt(stats.Size(), 10)

	w.WriteHeader(status)
	w.Header().Set("Content-Disposition", "attachment; filename="+stats.Name())
	w.Header().Set("Content-Type", fileContentType)
	w.Header().Set("Content-Length", size)

	_, _ = file.Seek(0, 0)
	_, _ = io.Copy(w, file)
}

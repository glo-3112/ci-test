module api

go 1.13

require (
	github.com/gorilla/mux v1.7.3
	github.com/kr/pretty v0.2.0 // indirect
	github.com/nyaruka/phonenumbers v1.0.54
	github.com/rs/cors v1.7.0
	github.com/satori/go.uuid v1.2.0
	github.com/stretchr/testify v1.4.0 // indirect
	golang.org/x/crypto v0.0.0-20200115085410-6d4e4cb37c7d
	golang.org/x/image v0.0.0-20200119044424-58c23975cae1
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
	gopkg.in/square/go-jose.v2 v2.4.1
	gopkg.in/yaml.v2 v2.2.8 // indirect
)

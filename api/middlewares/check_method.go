package middlewares

import (
	"net/http"

	"api/generic/api"
	"api/req"
	"api/tools"
)

func CheckMethod(allowedMethods ...string) tools.MdFunc {
	return func(s *req.Session, w req.Writer, r *req.Reader, next tools.CtrlFunc) {
		for _, test := range allowedMethods {
			if r.Method() == test {
				next(s, w, r)
				return
			}
		}
		api.SendResponse(w, http.StatusMethodNotAllowed, http.StatusText(http.StatusMethodNotAllowed), nil)
		return
	}
}

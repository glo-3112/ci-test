package middlewares

import (
	"net/http"

	"api/generic/api"
	"api/req"
	"api/tools"
)

func HasJsonBody(s *req.Session, w req.Writer, r *req.Reader, next tools.CtrlFunc) {
	content := r.Header().Get("Content-Type")

	if content != "application/json" {
		api.SendResponse(w, http.StatusNotAcceptable, http.StatusText(http.StatusNotAcceptable), nil)
		return
	}

	next(s, w, r)
	return
}

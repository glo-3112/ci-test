package middlewares

import (
	"net/http"

	"api/generic/api"
	"api/req"
	"api/tools"
)

func HasPathParam(params []string) tools.MdFunc {
	return func(s *req.Session, w req.Writer, r *req.Reader, next tools.CtrlFunc) {
		for _, param := range params {
			if val, ok := r.Params[param]; !ok || val == "" {
				api.SendResponse(w, http.StatusBadRequest, http.StatusText(http.StatusBadRequest) + " " + param + " is required", nil)
				return
			}
		}

		next(s, w, r)
	}
}

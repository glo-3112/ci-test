package middlewares

import (
	"api/generic/api"
	"api/req"
	"api/tools"
)

func HasRole(roleNames ...string) tools.MdFunc {
	return func(s *req.Session, w req.Writer, r *req.Reader, next tools.CtrlFunc) {
		if s.Token == nil || s.Token.User == nil {
			api.SendForbidden(w)
			return
		}
		userRoles := s.Token.User.Roles

		for _, name := range roleNames {
			for _, role := range userRoles {
				if role == name {
					next(s, w, r)
					return
				}
			}
		}
		api.SendForbidden(w, "Role not high enough")
	}
}

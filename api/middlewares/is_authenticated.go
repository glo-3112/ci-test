package middlewares

import (
	"net/http"
	"time"

	uuid "github.com/satori/go.uuid"

	"api/generic/api"
	"api/repository"
	"api/req"
	"api/tools"
	"api/tools/console"

	securityConst "api/constants/security"
	security "api/security/jwt"
)

func ShouldBeAuthenticated(s *req.Session, w req.Writer, r *req.Reader, next tools.CtrlFunc) {
	token, err := r.Cookie(securityConst.TokenCookieName)
	if err != nil || token.Value == "" {
		console.Log("Token cookie", err)
		api.SendUnauthorized(w, "Bad token cookie")
		return
	}

	_, err = r.Cookie(securityConst.AuthenticationCookieName)
	if err != nil {
		console.Log("Authentication cookie", err)
		api.SendBadRequest(w, "Bad authentication cookie")
		return
	}

	claims, err := security.ParseJwt(token.Value)
	if err != nil {
		console.Log(err)
		api.SendBadRequest(w, "Bad authentication token")
		return
	}

	uid, err := uuid.FromString(claims.ID)
	if claims.Issuer != securityConst.JWTIssuer || err != nil {
		api.SendBadRequest(w, "Bad authentication token 2")
		return
	}

	if claims.Expiry.Time().Unix() < time.Now().Unix() {
		api.SendUnauthorized(w, "Authentication expired")
		return
	}

	tokenInfo, err := repository.TokenRepository.FindByUUID(&uid)
	if err != nil {
		console.Log(err)
		api.SendUnauthorized(w, "Unknown token")
		return
	}
	repository.TokenRepository.ResolveNested(1, tokenInfo)

	if claims.IssuedAt.Time().Unix() != tokenInfo.IssuedAt.Unix() {
		api.SendResponse(w, http.StatusUnauthorized, "The given token has expired", nil)
		return
	}

	if claims.UserId != tokenInfo.UserId {
		api.SendResponse(w, http.StatusUnauthorized, "Token error", nil)
		return
	}

	s.Token = tokenInfo
	s.User = tokenInfo.User
	next(s, w, r)
	return
}

func ResolveAuthentication(s *req.Session, w req.Writer, r *req.Reader, next tools.CtrlFunc) {
	token, err := r.Cookie(securityConst.TokenCookieName)
	if err != nil || token.Value == "" {
		next(s, w, r)
		return
	}

	_, err = r.Cookie(securityConst.AuthenticationCookieName)
	if err != nil {
		next(s, w, r)
		return
	}

	claims, err := security.ParseJwt(token.Value)
	if err != nil {
		console.Log(err)
		api.SendUnauthorized(w, "Bad authentication token")
		return
	}

	uid, err := uuid.FromString(claims.ID)
	if claims.Issuer != securityConst.JWTIssuer || err != nil {
		api.SendBadRequest(w, "Bad token 2")
		return
	}

	if claims.Expiry.Time().Unix() < time.Now().Unix() {
		next(s, w, r)
		return
	}

	tokenInfo, err := repository.TokenRepository.FindByUUID(&uid)
	if err != nil {
		next(s, w, r)
		return
	}
	repository.TokenRepository.ResolveNested(1, tokenInfo)

	if claims.IssuedAt.Time().Unix() != tokenInfo.IssuedAt.Unix() {
		api.SendResponse(w, http.StatusUnauthorized, "The given token has expired", nil)
		return
	}

	if claims.UserId != tokenInfo.UserId {
		api.SendResponse(w, http.StatusUnauthorized, "Token error", nil)
		return
	}

	s.Token = tokenInfo
	s.User = tokenInfo.User
	next(s, w, r)
	return
}
package middlewares

import (
	"net/http"

	"api/generic/api"
	"api/req"
	"api/tools"
)

type Methods map[string][]tools.MdFunc

func MultiMethod(routes Methods) tools.MdFunc {
	return func(s *req.Session, w req.Writer, r *req.Reader, next tools.CtrlFunc) {
		if method, ok := routes[r.Method()]; ok {
			md := tools.MiddlewareResolver(method)
			md(s, w, r)
		} else {
			api.SendResponse(w, http.StatusMethodNotAllowed, http.StatusText(http.StatusMethodNotAllowed), nil)
		}
	}
}

package middlewares

import (
	"api/generic/api"
	"api/models"
	"api/req"
	"api/tools"
)

func PrivacyProtected(s *req.Session, w req.Writer, r *req.Reader, next tools.CtrlFunc) {

	if r.SolvedParams["target_user"] == nil {
		api.SendUnauthorized(w, "This profile is not public 1")
		return
	}
	targetUser := r.SolvedParams["target_user"].(*models.User)

	if s.User != nil && s.User.Id == targetUser.Id {
		goto friendship
	}

	if !targetUser.Public {
		if s.User == nil {
			api.SendUnauthorized(w, "This profile is not public 2")
			return
		}

		for _, v := range targetUser.FriendsIds {
			if v == s.User.Id {
				goto friendship
			}
		}
		api.SendUnauthorized(w, "This profile is not public 3")
		return
	}

friendship:
	next(s, w, r)
}

package middlewares

import (
	"api/generic/api"
	"api/repository"
	"api/req"
	"api/tools"
)

func ShouldOwnPublication(s *req.Session, w req.Writer, r *req.Reader, next tools.CtrlFunc) {
	user := s.User
	if user == nil {
		api.SendForbidden(w, "You should be authenticated to do this action")
		return
	}

	publicationId := r.Params["publicationId"]
	if publicationId == "" {
		api.SendBadRequest(w)
		return
	}

	publication, err := repository.PublicationsRepository.FindByAuthorAndId(user.Id.Hex(), publicationId)
	if err != nil {
		api.SendInternalError(w, "An uknown error occured")
		return
	} else if publication == nil {
		api.SendNotFound(w, "Publication", publicationId, "not found")
		return
	}

	r.SolvedParams["publication"] = publication
	next(s, w, r)
}

func ResolvePublication(s *req.Session, w req.Writer, r *req.Reader, next tools.CtrlFunc) {
	publicationId := r.Params["publicationId"]
	if publicationId == "" {
		api.SendBadRequest(w)
		return
	}

	publication, err := repository.PublicationsRepository.FindById(publicationId)
	if err != nil {
		api.SendInternalError(w, "An unknown error occured")
		return
	} else if publication == nil {
		api.SendNotFound(w, "Publication", publicationId, "not found")
		return
	}

	author, err := repository.UserRepository.FindByObjectId(publication.Author)
	if err != nil {
		api.SendInternalError(w, "An unknown error occurred")
		return
	}

	r.SolvedParams["target_user"] = author
	r.SolvedParams["publication"] = publication
	next(s, w, r)
}
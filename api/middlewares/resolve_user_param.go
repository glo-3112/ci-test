package middlewares

import (
	"gopkg.in/mgo.v2"

	"api/generic/api"
	"api/repository"
	"api/req"
	"api/tools"
)

func ResolveUserParam(s *req.Session, w req.Writer, r *req.Reader, next tools.CtrlFunc) {
	userId := r.Params["userId"]

	if userId == "" {
		api.SendNotFound(w, "Userid", userId, "not found")
		return
	}

	user, err := repository.UserRepository.FindByIdOrUsername(userId)
	if err != nil && err != mgo.ErrNotFound {
		api.SendInternalError(w, "Unknown error occured")
		return
	} else if user == nil {
		api.SendNotFound(w,"Userid", userId, "not found")
		return
	}

	r.SolvedParams["user"] = user
	r.SolvedParams["target_user"] = user
	next(s, w, r)
}

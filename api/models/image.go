package models

type Image struct {
	MdThumb string `json:"mdThumb"`
	SmThumb string `json:"smThumb"`
	Normal string `json:"normal"`
}
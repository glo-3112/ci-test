package models

type PhoneNumber struct {
	ISOCode       string `bson:"iso_code" json:"isoCode"`
	Number        string `bson:"number" json:"number"`
	International string `bson:"international" json:"international,omitempty"`
}

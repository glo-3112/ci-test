package models

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

type Publication struct {
	Id          bson.ObjectId   `bson:"_id" json:"id,omitempty"`
	Author      bson.ObjectId   `bson:"author" json:"author,omitempty"`
	Description *string         `bson:"description" json:"description,omitempty"`
	Tags        []string        `bson:"tags" json:"tags,omitempty"`
	MentionsId  []bson.ObjectId `bson:"mentions_id" json:"mentionsId,omitempty"`
	Mentions    []UserPublic    `bson:"-" json:"mentions,omitempty"`
	Images      []bson.ObjectId `bson:"images" json:"images,omitempty"`
	CreatedAt   time.Time       `bson:"created_at" json:"createdAt,omitempty"`
	UpdatedAt   time.Time       `bson:"updated_at" json:"updatedAt,omitempty"`
}

type PublicationImage struct {
	Id   bson.ObjectId `bson:"_id" json:"id"`
	Name string        `bson:"name" json:"name"`
}

type PublicationCreate struct {
	Description string          `json:"description"`
	Tags        []string        `json:"tags"`
	MentionsId  []bson.ObjectId `json:"mentionsId,omitempty"`
}

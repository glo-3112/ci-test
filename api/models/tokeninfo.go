package models

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

type TokenInfo struct {
	Id       bson.ObjectId `bson:"_id" json:"id,omitempty"`
	Uuid     string        `bson:"uuid" json:"uuid"`
	Expiry   time.Time     `bson:"expiry" json:"expiry"`
	IssuedAt time.Time     `bson:"issued_at" json:"issuedAt"`
	UserId   bson.ObjectId `bson:"user_id" json:"userId"`
	User     *User         `bson:"user,omitempty" json:"user,omitempty"`
}

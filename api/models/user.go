package models

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

type User struct {
	Id          bson.ObjectId `bson:"_id" json:"id,omitempty"`
	Roles       []string      `bson:"roles" json:"roles"`
	Name        string        `bson:"name" json:"username"`
	Hash        string        `bson:"hash" json:"-"`
	Firstname   string        `bson:"firstname" json:"firstName,omitempty"`
	Lastname    string        `bson:"lastname" json:"lastName,omitempty"`
	Mail        string        `bson:"mail" json:"mail"`
	PhoneNumber PhoneNumber   `bson:"phone_number" json:"phoneNumber"`

	Public bool `bson:"public" json:"public"`

	// This field contain all the user's friends id
	//   This is a private field (aka for mongo only)
	//   It should never be serialized
	FriendsIds []bson.ObjectId `bson:"friends_ids" json:"friendsId,omitempty"`

	// This field can contain both an array of ids or an array of PublicUser
	// It's the public version of FriendsId
	Friends interface{} `bson:"-" json:"friends"`

	// A foreign key to Pictures image
	ProfilePicture *bson.ObjectId `bson:"profile_picture" json:"profilePicture"`

	// This field contain all the publication of the user
	PublicationIds []bson.ObjectId `bson:"publication_ids" json:"publicationsId,omitempty"`
	Publications   []Publication   `bson:"-" json:"publications,omitempty"`

	RegistrationDate time.Time `bson:"registration_date" json:"registrationDate,omitempty"`
	LastSignIn       time.Time `bson:"last_sign_in" json:"lastSignIn,omitempty"`
}

type PartialUser struct {
	Name        *string      `json:"username,omitempty"`
	Firstname   *string      `json:"firstName,omitempty"`
	Lastname    *string      `json:"lastName,omitempty"`
	Mail        *string      `json:"mail,omitempty"`
	PhoneNumber *PhoneNumber `json:"phoneNumber,omitempty"`
}

type UserPublic struct {
	Id        bson.ObjectId `bson:"_id" json:"id"`
	Name      string        `bson:"name" json:"username"`
	Firstname string        `bson:"firstname" json:"firstName,omitempty"`
	Lastname  string        `bson:"lastname" json:"lastName,omitempty"`

	// This field contain all the user's friends id
	//   This is a private field (aka for mongo only)
	//   It should never be serialized
	FriendsIds []bson.ObjectId `bson:"friends_ids" json:"friendsId,omitempty"`

	// This field contain all the publication of the user
	PublicationIds []bson.ObjectId `bson:"publication_ids" json:"publicationsId,omitempty"`

	ProfilePicture   *bson.ObjectId `bson:"profile_picture" json:"profilePicture"`
	RegistrationDate time.Time      `bson:"registration_date" json:"registrationDate,omitempty"`
}

type UserMail struct {
	Mail string `json:"mail"`
}

type UserFirstname struct {
	Firstname string `json:"firstName"`
}

type UserLastname struct {
	Lastname string `json:"lastName"`
}

type UserName struct {
	UserFirstname
	UserLastname
}

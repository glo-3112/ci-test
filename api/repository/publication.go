package repository

import (
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	dbconsts "api/constants/database"
	"api/models"
)

type dbPublications struct {
	collection *mgo.Collection
}

var PublicationsRepository dbPublications

func init() {
	PublicationsRepository = dbPublications{}
	PublicationsRepository.Connect()
}

func (dbPub *dbPublications) Connect() {
	dbPub.collection = Db.C(dbconsts.PublicationsCollection)
}

func (dbPub *dbPublications) CreatePublication(user *models.User, form models.PublicationCreate) (*models.Publication, error) {
	insert := &models.Publication{
		Id:          bson.NewObjectId(),
		Author:      user.Id,
		Description: &form.Description,
		Tags:        form.Tags,
		MentionsId:  form.MentionsId,
		Images:      []bson.ObjectId{},
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	}

	err := dbPub.collection.Insert(insert)
	if err != nil {
		return nil, err
	}
	user.PublicationIds = append(user.PublicationIds, insert.Id)

	if err := UserRepository.Persist(user); err != nil {
		return nil, err
	}

	return insert, nil
}

func (dbPub *dbPublications) FindById(id string) (*models.Publication, error) {
	publication := &models.Publication{}

	err := dbPub.collection.Find(bson.M{
		"_id": bson.ObjectIdHex(id),
	}).One(publication)
	if err != nil && err != mgo.ErrNotFound {
		return nil, err
	} else if err == mgo.ErrNotFound {
		return nil, nil
	}

	return publication, nil
}

func (dbPub *dbPublications) FindByAuthorAndId(authorId string, id string) (*models.Publication, error) {
	publication := &models.Publication{}

	err := dbPub.collection.Find(bson.M{
		"_id":    bson.ObjectIdHex(id),
		"author": bson.ObjectIdHex(authorId),
	}).One(publication)
	if err != nil && err != mgo.ErrNotFound {
		return nil, err
	} else if err == mgo.ErrNotFound {
		return nil, nil
	}

	return publication, nil
}

func (dbPub *dbPublications) FindUserPublicationsPaged(userId bson.ObjectId, page, size int) ([]models.Publication, error) {
	publications := []models.Publication{}

	err := dbPub.collection.Find(bson.M{
		"author": userId,
	}).Sort("-created_at").Skip((page - 1) * size).Limit(size).All(&publications)
	if err != nil {
		return nil, err
	}

	return publications, nil
}

func (dbPub *dbPublications) Persist(publication *models.Publication) (*models.Publication, error) {

	err := dbPub.collection.Update(bson.M{
		"_id": publication.Id,
	}, publication)
	if err != nil {
		return nil, err
	}

	return publication, nil
}

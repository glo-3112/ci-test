package repository

import (
	"fmt"
	"time"

	"golang.org/x/crypto/bcrypt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	dbconst "api/constants/database"
	userConst "api/constants/users"
	"api/form/api/v1"
	"api/models"
)

type DbUsers struct {
	collection *mgo.Collection
}

var UserRepository DbUsers

func init() {
	UserRepository = DbUsers{}
	UserRepository.connect()
}

func (dbusr *DbUsers) connect() {
	dbusr.collection = Db.C(dbconst.UsersCollection)
}

func (dbusr *DbUsers) Find() ([]*models.User, error) {
	users := []*models.User{}

	err := dbusr.collection.Find(bson.M{}).All(&users)
	if err != nil {
		return nil, err
	}
	return users, nil
}

func (dbusr *DbUsers) FindById(id string) (*models.User, error) {
	return dbusr.FindByObjectId(bson.ObjectIdHex(id))
}

func (dbusr *DbUsers) FindByObjectId(id bson.ObjectId) (*models.User, error) {
	user := &models.User{}

	err := dbusr.collection.Find(bson.M{"_id": id}).One(user)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (dbusr *DbUsers) FindByIdOrUsername(query string) (*models.User, error) {
	user := &models.User{}

	err := dbusr.collection.Find(bson.M{
		"$or": []bson.M{
			{
				"_id": bson.ObjectIdHex(query),
			},
			{
				"name": query,
			},
		},
	}).One(user)
	if err != nil && err != mgo.ErrNotFound {
		return nil, err
	} else if err == mgo.ErrNotFound {
		return nil, nil
	}

	return user, nil
}

func (dbusr *DbUsers) FindByName(name string) (*models.User, error) {
	user := &models.User{}

	err := dbusr.collection.Find(bson.M{"name": name}).One(user)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (dbusr *DbUsers) FindByMail(mail string) (*models.User, error) {
	user := &models.User{}

	err := dbusr.collection.Find(bson.M{"mail": mail}).One(user)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (dbusr *DbUsers) IsMailAvailable(mail string) (bool, error) {
	nb, err := dbusr.collection.Find(bson.M{
		"mail": bson.M{
			"$eq": mail,
		},
	}).Count()

	if err != nil {
		return false, err
	}
	return nb == 0, nil
}

func (dbusr *DbUsers) IsPhoneAvailable(phone models.PhoneNumber) (bool, error) {
	nb, err := dbusr.collection.Find(bson.M{
		"$and": []bson.M{
			{
				"phone_number.number": bson.M{
					"$eq": phone.Number,
				},
			},
			{
				"phone_number.iso_code": bson.M{
					"$eq": phone.ISOCode,
				},
			},
		},
	}).Count()

	if err != nil {
		return false, err
	} else {
		return nb == 0, nil
	}
}

func (dbusr *DbUsers) IsUsernameOrMailAvailable(username string, mail string) (bool, error) {
	nb, err := dbusr.collection.Find(bson.M{
		"$or": []bson.M{
			{"name": username},
			{"mail": mail},
		},
	}).Count()

	if err != nil {
		return false, err
	}
	if nb == 0 {
		return true, nil
	}
	return false, nil
}

func (dbusr *DbUsers) FindByUsernameOrMail(username string, mail string) (*models.User, error) {
	user := &models.User{}

	err := dbusr.collection.Find(bson.M{"$or": []bson.M{{"name": username}, {"mail": mail}}}).One(user)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (dbusr *DbUsers) FindByLogin(login string) (*models.User, error) {
	return dbusr.FindByUsernameOrMail(login, login)
}

func (dbusr *DbUsers) FindByLoginAndPassword(login string, password string) (*models.User, error) {
	user, err := dbusr.FindByLogin(login)

	if err != nil {
		return nil, err
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Hash), []byte(password))
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (dbusr *DbUsers) CreateUserNoHashPassword(user form.SignUp) (*models.User, error) {

	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}

	insert := &models.User{
		Id:             bson.NewObjectId(),
		Roles:          []string{userConst.DefaultUserRole},
		Hash:           string(hash),
		Name:           user.Username,
		Firstname:      user.Firstname,
		Lastname:       user.Lastname,
		Mail:           user.Mail,
		PhoneNumber:    user.PhoneNumber,
		ProfilePicture: nil,

		Public: true,

		FriendsIds: []bson.ObjectId{},

		PublicationIds: []bson.ObjectId{},

		LastSignIn:       time.Now(),
		RegistrationDate: time.Now(),
	}

	err = dbusr.collection.Insert(insert)
	if err != nil {
		return nil, err
	}

	return insert, nil
}

func (dbusr *DbUsers) UpdateByID(id bson.ObjectId, user *models.User) error {
	return dbusr.collection.Update(bson.M{"_id": id}, user)
}

func (dbusr *DbUsers) UpdateLastSignIn(user *models.User) error {
	user.LastSignIn = time.Now()
	return dbusr.collection.Update(bson.M{"_id": user.Id}, user)
}

func (dbusr *DbUsers) Persist(user *models.User) error {
	return dbusr.UpdateByID(user.Id, user)
}

func (dbusr *DbUsers) ResolveNested(level int, user *models.User) *models.User {
	return user
}

func (dbusr *DbUsers) FindPaged(page int, size int) ([]*models.User, error) {
	users := []*models.User{}

	err := dbusr.collection.Find(bson.M{}).Skip((page - 1) * size).Limit(page).All(users)
	if err != nil {
		return nil, err
	}
	return users, nil
}

func (dbusr *DbUsers) PublicFindPaged(page int, size int) ([]*models.UserPublic, error) {
	users := []*models.UserPublic{}

	err := dbusr.collection.Find(bson.M{

	}).Select(bson.M{
		"profile_picture":   1,
		"name":              1,
		"firstname":         1,
		"lastname":          1,
		"registration_date": 1,
	}).Skip((page - 1) * size).Limit(size).All(&users)
	fmt.Println(users, err)
	if err != nil {
		return nil, err
	}
	return users, nil
}

func (dbusr *DbUsers) PublicUserSearchPaged(query string, page int, size int) ([]*models.UserPublic, error) {

	startBy := "^" + query
	mgoQuery := []bson.M{
		{
			"$match": bson.M{
				"$or": []bson.M{
					{
						"name": bson.M{
							"$regex":   query,
							"$options": "ix",
						},
					},
					{
						"firstname": bson.M{
							"$regex":   query,
							"$options": "ix",
						},
					},
					{
						"lastname": bson.M{
							"$regex":   query,
							"$options": "ix",
						},
					},
				},
			},
		},
		{
			"$set": bson.M{
				"common": bson.M{
					"$cond": []interface{}{
						bson.M{
							"$regexMatch": bson.M{
								"input":   "$name",
								"regex":   "des",
								"options": "ix",
							},
						}, "$name", bson.M{
							"$cond": []interface{}{
								bson.M{
									"$regexMatch": bson.M{
										"input":   "$firstname",
										"regex":   "des",
										"options": "ix",
									},
								}, "$firstname", "$lastname"},
						},
					},
				},
				"matchType": bson.M{
					"$cond": []interface{}{
						bson.M{
							"$regexMatch": bson.M{
								"input":   "$name",
								"regex":   startBy,
								"options": "ix",
							},
						}, 6, bson.M{
							"$cond": []interface{}{
								bson.M{
									"$regexMatch": bson.M{
										"input":   "$firstname",
										"regex":   startBy,
										"options": "ix",
									},
								}, 5, bson.M{
									"$cond": []interface{}{
										bson.M{
											"$regexMatch": bson.M{
												"input":   "$lastname",
												"regex":   startBy,
												"options": "ix",
											},
										}, 4, bson.M{
											"$cond": []interface{}{
												bson.M{
													"$regexMatch": bson.M{
														"input":   "$name",
														"regex":   query,
														"options": "ix",
													},
												}, 3, bson.M{
													"$cond": []interface{}{
														bson.M{
															"$regexMatch": bson.M{
																"input":   "$firstname",
																"regex":   query,
																"options": "ix",
															},
														}, 2, bson.M{
															"$cond": []interface{}{
																bson.M{
																	"$regexMatch": bson.M{
																		"input":   "$lastname",
																		"regex":   query,
																		"options": "ix",
																	},
																}, 1, 0},
														},
													},
												},
											},
										},
									},
								},
							},
						},
					},
				},
			},
		}, {
			"$set": bson.M{
				"score": bson.M{
					"$subtract": []interface{}{
						100,
						bson.M{
							"$multiply": []interface{}{
								bson.M{
									"$divide": []interface{}{
										bson.M{
											"$subtract": []interface{}{
												bson.M{
													"$strLenCP": "$common",
												},
												len(query),
											},
										},
										bson.M{
											"$strLenCP": "$common",
										},
									},
								},
								100,
							},
						},
					},
				},
			},
		}, {
			"$sort": bson.M{
				"matchType": -1,
				"score":     -1,
			},
		}, {
			"$project": bson.M{
				"matchType":         0,
				"roles":             0,
				"hash":              0,
				"mail":              0,
				"phone_number":      0,
				"last_sign_in":      0,
				"common":            0,
				"registration_date": 0,
			},
		}, {
			"$skip": (page - 1) * size,
		}, {
			"$limit": size,
		},
	}
	users := []*models.UserPublic{}

	err := dbusr.collection.Pipe(mgoQuery).All(&users)

	if err != nil {
		return nil, err
	}
	return users, nil
}

func (dbusr *DbUsers) PublicFindIds(ids []bson.ObjectId) ([]*models.UserPublic, error) {
	users := []*models.UserPublic{}

	err := dbusr.collection.Find(bson.M{
		"_id": bson.M{
			"$in": ids,
		},
	}).All(&users)

	if err != nil {
		return nil, err
	}
	return users, nil
}

func (dbusr *DbUsers) CountUserIds(ids []bson.ObjectId) (int, error) {

	n, err := dbusr.collection.Find(bson.M{
		"_id": bson.M{
			"$in": ids,
		},
	}).Count()

	if err != nil {
		return 0, err
	}

	return n, nil
}

func (dbusr *DbUsers) GetPublicData(user *models.User) *models.UserPublic {
	return &models.UserPublic{
		Id:               user.Id,
		Name:             user.Name,
		Firstname:        user.Firstname,
		Lastname:         user.Lastname,
		ProfilePicture:   user.ProfilePicture,
		FriendsIds:       user.FriendsIds,
		PublicationIds:   user.PublicationIds,
		RegistrationDate: user.RegistrationDate,
	}
}

func (dbusr *DbUsers) GetPrivacyPublicData(user *models.User) *models.UserPublic {
	return &models.UserPublic{
		Id:               user.Id,
		Name:             user.Name,
		Firstname:        user.Firstname,
		Lastname:         user.Lastname,
		ProfilePicture:   user.ProfilePicture,
		RegistrationDate: user.RegistrationDate,
	}
}
package req

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"reflect"
	"strconv"

	apiErr "api/constants/errors"
)

type Reader struct {
	Req *http.Request

	Params map[string]string
	SolvedParams map[string]interface{}

	data interface{}
}

type Paged struct {
	Page int `json:"page"`
	Size int `json:"size"`
	Next *string `json:"next"`
	Prev *string `json:"prev"`
	Items interface{} `json:"items"`
}

func (r *Reader)UnmarshalJSON(target interface{}) error {
	body, err := ioutil.ReadAll(r.Req.Body)
	if err != nil {
		return errors.New(apiErr.ErrNoBodyProvided)
	}

	err = json.Unmarshal(body, target)
	if err != nil {
		return errors.New(apiErr.ErrBadBodyFormat)
	}

	return nil
}

func (r *Reader) Method() string {
	return r.Req.Method
}

func (r *Reader) Cookie(name string) (*http.Cookie, error) {
	return r.Req.Cookie(name)
}

func (r *Reader) Cookies() []*http.Cookie {
	return r.Req.Cookies()
}

func (r *Reader) Header() http.Header {
	return r.Req.Header
}

func (r *Reader) Paged(defaultPage int, defaultSize int) (int, int) {
	page := defaultPage
	size := defaultSize

	if s := r.Req.URL.Query().Get("page"); s != "" {
		if v, err := strconv.ParseInt(s, 10, 32); err != nil {
			size = defaultPage
		} else if v > 0 {
			page = int(v)
		}
	}
	if s := r.Req.URL.Query().Get("size"); s != "" {
		if v, err := strconv.ParseInt(s, 10, 32); err != nil {
			size = defaultSize
		} else if v > 0 {
			size = int(v)
		}
	}
	return page, size
}

func (r *Reader) MakePagedResponse(page int, size int, data interface{}) Paged {

	uri, _ := url.Parse(r.Req.URL.String())
	values := uri.Query()

	values.Set("page", strconv.Itoa(page + 1))
	values.Set("size", strconv.Itoa(size))

	ret := Paged{
		Page: page,
		Size: size,
		Items: data,
		Next: nil,
		Prev: nil,
	}

	val := reflect.ValueOf(data)
	if val.Len() >= size {
		u := uri.Host + uri.Path + "?" + values.Encode()
		ret.Next = &u
	}

	if page > 1 {
		values.Set("page", strconv.Itoa(page - 1))
		u := uri.Host + uri.Path + "?" + values.Encode()
		ret.Prev = &u
	}

	return ret
}
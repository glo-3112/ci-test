package req

import (
	"time"

	"api/models"
)

type Session struct {
	Token        *models.TokenInfo
	User         *models.User
	RequestStart time.Time
}

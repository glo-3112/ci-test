package req

import (
	"net/http"
	"time"
)

type Writer struct {
	http.ResponseWriter
	RequestTime time.Time
}
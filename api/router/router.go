package router

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/gorilla/mux"

	"api/req"
	"api/tools"
)

type route struct {
	path string
	handler http.HandlerFunc
}

type Router struct {
	Basepath string
	routes   []route
}

// Create a router with basepath as starting point
func NewRouter(basepath string) *Router {
	ret := &Router{Basepath: basepath}
	ret.routes = []route{}
	return ret
}

// Register the given middleware to a route
func (r *Router) Register(path string, middleware ...tools.MdFunc) {

	handler := tools.MiddlewareResolver(middleware)

	path = strings.TrimRight(path, "/")
	route := route{path: r.Basepath + path}
	route.handler = func(w http.ResponseWriter, r *http.Request) {
		wr := req.Writer{ResponseWriter: w, RequestTime: time.Now()}
		rd := &req.Reader{Req: r, SolvedParams: map[string]interface{}{}, Params: mux.Vars(r)}
		handler(&req.Session{}, wr, rd)
	}
	r.routes = append(r.routes, route)
}

// Include the given router routes into this router
func (r *Router) Use(router *Router) {
	for _, route := range router.routes {
		route.path = r.Basepath + route.path
		r.routes = append(r.routes, route)
	}
}

// Apply all the routes to the go router.
func (r *Router) Apply(mux *mux.Router) {
	for _, route := range r.routes {
		fmt.Println("applying route", route.path)
		mux.HandleFunc(route.path, route.handler)
	}
}

package security

import (
	"gopkg.in/mgo.v2/bson"
	"gopkg.in/square/go-jose.v2"
	"gopkg.in/square/go-jose.v2/jwt"

	securityconst "api/constants/security"
	"api/models"
	"api/repository"
)

var signingKey = jose.SigningKey{Algorithm: jose.HS512, Key: []byte(securityconst.JWTSigningKey)}

func CreateJwt(user models.User) string {
	signer, err := jose.NewSigner(signingKey, (&jose.SignerOptions{}).WithType("JWT"))
	if err != nil {
		panic(err)
	}

	claims := CreateClaims(user)
	token, err := jwt.Signed(signer).Claims(claims).CompactSerialize()
	if err != nil {
		panic(err)
	}

	infos := &models.TokenInfo{
		Id:       bson.NewObjectId(),
		Uuid:     claims.ID,
		Expiry:   claims.Expiry.Time(),
		IssuedAt: claims.IssuedAt.Time(),
		UserId:   user.Id,
	}

	_, err = repository.TokenRepository.Insert(infos)
	if err != nil {
		panic(err)
	}

	return token
}

package console

import (
	"fmt"
	"os"
	"time"
)

var logs *os.File = nil

func init() {
	var err error

	logs, err = os.Create("./logs-" + time.Now().String() + ".out")
	if err != nil {
		panic(err)
	}
}

func Log(a ...interface{}) string {

	logId := time.Now()

	_, _ = logs.WriteString(fmt.Sprintln() + fmt.Sprintln(logId.String(), ":"))
	for _, v := range a {
		_, _ = logs.WriteString(fmt.Sprintln(v))
	}

	return logId.String()
}

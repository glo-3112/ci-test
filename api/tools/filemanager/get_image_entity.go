package filemanager

import (
	"path"

	"api/models"
)

func GetImageEntity(filename string) models.Image {
	return models.Image{
		MdThumb: path.Join("/static/medium", filename),
		SmThumb: path.Join("/static/small", filename),
		Normal:  path.Join("/static/original", filename),
	}
}

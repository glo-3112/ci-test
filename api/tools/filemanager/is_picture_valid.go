package filemanager

import (
	"errors"
	"mime/multipart"
)

func IsPictureValid(header *multipart.FileHeader) error {
	switch header.Header.Get("Content-Type") {
	case "image/jpeg":
	case "image/png":
	case "image/tiff":
	case "image/gif":
	case "image/bmp":
	case "image/webp":
		break
	default:
		return errors.New("Wrong file format")
	}

	if header.Size > 10<<20 {
		return errors.New("File too big")
	}

	return nil
}

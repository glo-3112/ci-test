package filemanager

import (
	"net/http"
	"os"

	"api/constants"
)

func GetMimeType(file *os.File) (string, error) {
	buffer := make([]byte, 512)

	_, err := file.Read(buffer)
	if err != nil {
		return "", err
	}

	contentType := http.DetectContentType(buffer)

	return contentType, nil
}

func IsImageMimeTypeAllowed(mime string) bool {
	return IsMimeTypeOneOf(mime, constants.GetAllowedImageMimeTypes())
}

func IsMimeTypeOneOf(expected string, mimes ... []string) bool {
	for _, list := range mimes {
		for _, mime := range list {
			if mime == expected {
				return true
			}
		}
	}

	return false
}
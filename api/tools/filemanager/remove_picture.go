package filemanager

import (
	"os"
	"path"

	"api/constants"
)

func RemovePicturesByFolderAndName(folder string, pictures ...string) error {
	var err error = nil

	for _, picture := range pictures {
		e := os.Remove(path.Join(constants.GetStaticDir(), folder, picture))
		if e != nil && err == nil {
			err = e
		}
	}

	return err
}

package tools

import (
	"api/req"
)

type MdFunc func(s *req.Session, w req.Writer, r *req.Reader, next CtrlFunc)
type CtrlFunc func(s *req.Session, w req.Writer, r *req.Reader)

var nullcb CtrlFunc = func(s *req.Session, w req.Writer, r *req.Reader) {}

func buildChain(a MdFunc, b []MdFunc) CtrlFunc {
	c := nullcb
	if len(b) > 0 {
		c = buildChain(b[0], b[1:])
	}

	return func(s *req.Session, w req.Writer, r *req.Reader) {
		a(s, w, r, c)
	}
}

func MiddlewareResolver(ints []MdFunc) CtrlFunc {

	return buildChain(ints[0], ints[1:])
}

package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"

	"api/constants"
	"api/controllers/api"
	"api/controllers/static"
	api2 "api/generic/api"
	"api/middlewares"
	"api/req"
	"api/router"
)

func main() {
	ugramServer := mux.NewRouter()

	mainRouter := router.NewRouter("")
	mainRouter.Use(api.ApiRouter)
	mainRouter.Use(static.StaticRouter)
	mainRouter.Apply(ugramServer)

	cors := middlewares.SetupCors()

	if ugramServer == nil {
		log.Fatal("Error while creating the server")
	}
	ugramServer.StrictSlash(false)
	ugramServer.NotFoundHandler = notFoundHandler{}

	server := cors.Handler(ugramServer)

	fmt.Println("Starting server on", os.Getenv(constants.ListenPort))

	if err := http.ListenAndServe(":" + os.Getenv(constants.ListenPort), server); err != nil {
		log.Fatal(err)
	}
}

type notFoundHandler struct {
}

func (h notFoundHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	api2.SendResponse(req.Writer{ResponseWriter: w, RequestTime: time.Now()}, http.StatusNotFound, "Resource not found", nil)
}

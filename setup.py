#!/usr/bin/env python3

import sys
import string
import random

def randomString(stringLength=64):
    """Generate a random string of fixed length """
    letters = string.ascii_letters + string.digits + string.digits
    return ''.join(random.choice(letters) for i in range(stringLength))

def parseEnvFile(file, vars = {}, fin = {}):
    ret = vars.copy()
    try:
        f = open(file)
    except IOError:
        return ret
    with f:
        for line in f:
            if line.startswith('#'):
                continue
            value = line.strip().split('=', 1)
            if len(value) == 2:
                ret[value[0]] = value[1] # Save to a list
                fin[value[0]] = value[1] # Save to a list
        f.close()
        return ret

def writeEnvFile(file, vars = {}):
    with open(file, "w") as f:
        for key in vars:
            f.write(key + "=" + vars[key])
            f.write("\n")
        f.close()

file_api = {}
file_mongo = {}
file_mongo_express = {}
base_api = {
    "VITURAL_PORT": "8080",
    "MONGODB_URI": "mongo:27017",
    "MONGODB_DATABASE": "admin",
    "MONGODB_USERNAME": randomString(8),
    "MONGODB_PASSWORD": randomString(),
}

base_mongo = {
    "MONGO_INITDB_ROOT_USERNAME": base_api["MONGODB_USERNAME"],
    "MONGO_INITDB_ROOT_PASSWORD": base_api["MONGODB_PASSWORD"],
}

base_mongo_express = {
    "ME_CONFIG_BASICAUTH_USERNAME": "admin",
    "ME_CONFIG_BASICAUTH_PASSWORD": randomString(20),
    "ME_CONFIG_MONGODB_ENABLE_ADMIN": "true",
    "ME_CONFIG_MONGODB_ADMINUSERNAME": base_api["MONGODB_USERNAME"],
    "ME_CONFIG_MONGODB_ADMINPASSWORD": base_api["MONGODB_PASSWORD"],
}


def main():
    api = parseEnvFile("./env/api.env", base_api, file_api)
    mongo = parseEnvFile("./env/mongo.env", base_mongo, file_mongo)
    mongo_express = parseEnvFile("./env/mongo-express.env", base_mongo_express, file_mongo_express)
    writeEnvFile("./env/api.env", api)
    writeEnvFile("./env/mongo.env", mongo)
    writeEnvFile("./env/mongo-express.env", mongo_express)

    print("Mongodb credentials:")
    print("\033[31m(Changed)\033[39m  " if api["MONGODB_USERNAME"] != file_api["MONGODB_USERNAME"] else "\033[32m(Unchanged)\033[39m", "Username", api["MONGODB_USERNAME"])
    print("\033[31m(Changed)\033[39m  " if api["MONGODB_PASSWORD"] != file_api["MONGODB_PASSWORD"] else "\033[32m(Unchanged)\033[39m", "Password", api["MONGODB_PASSWORD"])
    print("")
    print("Mongo-express credentials: ")
    print("\033[31m(Changed)\033[39m" if mongo_express["ME_CONFIG_BASICAUTH_USERNAME"] != file_mongo_express["ME_CONFIG_BASICAUTH_USERNAME"] else "\033[32m(Unchanged)\033[39m", "Username", mongo_express["ME_CONFIG_BASICAUTH_USERNAME"])
    print("\033[31m(Changed)\033[39m" if mongo_express["ME_CONFIG_BASICAUTH_PASSWORD"] != file_mongo_express["ME_CONFIG_BASICAUTH_PASSWORD"] else "\033[32m(Unchanged)\033[39m", "Password", mongo_express["ME_CONFIG_BASICAUTH_PASSWORD"])

main()